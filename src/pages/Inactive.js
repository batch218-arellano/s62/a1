import CourseCard from '../components/CourseCard';

import {useState, useEffect} from 'react';

export default function RemoveProduct() {

	const [courses, setCourses] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data)
