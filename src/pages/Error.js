import Banner from '../components/Banner';

export default function Error() {

	const data = {
		title: "E-commerce",
		content: "Buy at the convenience of your home.",
		destination: "/Register",
		label: "Make a purchase today!"
	}

	return (
			<Banner data={data} />
			

		)
}