import Banner from '../components/Banner';
import Highlights from '../components/Highlights'; 

export default function Home() {

	const data = {
		title: "E-commerce",
		content: "Buy at the convenience of your home.",
		destination: "/Register",
		label: "Make a purchase today!"
	}
	return (
		<>
		<Banner data={data} />
    	<Highlights />
    	
		</>
	)
};

